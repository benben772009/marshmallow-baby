package com.honeymimi.utils;

import android.graphics.Canvas;
import android.view.SurfaceHolder;

public class GameThread extends Thread {
	protected IGameData _gd;
	protected SurfaceHolder _sh;
	protected boolean _running = false;
	protected int _interval;
	protected Runnable _cb = null;

	public GameThread (SurfaceHolder sv, IGameData gd) {
		_sh = sv;
		_gd = gd;
		_interval = _gd.getInterval();
	}
	
	public void setCb(Runnable cb) {
		_cb = cb;
	}
	
	public void run () {
		while (!_gd.isFinished() && _running == true) {
			Canvas c = null;
			try {
				c = _sh.lockCanvas();
				synchronized (_sh) {
					_gd.update();
					_gd.doDraw(c);
				}
				try {
					_sh.unlockCanvasAndPost(c);
				} catch (Exception e) {
					// TODO: handle exception
				}
			} finally {
				if (c != null) {
//					_sh.unlockCanvasAndPost(c);
				}
			}
			try {
				Thread.sleep(_interval);
			} catch (Exception e) {
				// TODO: handle exception
			}
		}
		if (_cb != null && _gd.isFinished()) {
			_cb.run();
		}
	}
	
	public void setRunning (boolean flag) {
		_running = flag;
	}
}
