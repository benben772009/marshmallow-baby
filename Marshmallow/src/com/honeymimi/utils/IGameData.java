package com.honeymimi.utils;

import android.graphics.Canvas;

public interface IGameData {
	void update();
	boolean isFinished();
	void doDraw(Canvas c);
	int getInterval();
}
