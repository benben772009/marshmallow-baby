package com.honeymimi.utils;

import android.graphics.Canvas;

public interface IDrawable {
	void draw(Canvas c);
}
