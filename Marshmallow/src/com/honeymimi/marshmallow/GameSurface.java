package com.honeymimi.marshmallow;

import com.honeymimi.marshmallow.gameData.MarshmallowLogic;
import com.honeymimi.marshmallow.gameData.MarshmallowStaticData;
import com.honeymimi.marshmallow.R;
import com.honeymimi.utils.GameThread;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.view.SurfaceHolder;
import android.view.SurfaceHolder.Callback;
import android.view.SurfaceView;

public class GameSurface extends SurfaceView implements Callback {
	private GameThread _thread = null;
	private Runnable _cb = null;

	public GameSurface (Context context) {
		super(context);
		Bitmap b = BitmapFactory.decodeResource(getResources(), R.drawable.tiles);
		MarshmallowStaticData.setBitmap(b);
		getHolder().addCallback(this);
		setFocusable(true);
	}
	
	public void setCb(Runnable cb) {
		_cb = cb;
		setThreadCb();
	}
	
	private void setThreadCb() {
		if (_thread != null && _cb != null) {
			_thread.setCb(_cb);
		}
	}

	@Override
	public void surfaceChanged(SurfaceHolder arg0, int arg1, int arg2, int arg3) {
		// TODO Auto-generated method stub

	}

	@Override
	public void surfaceCreated(SurfaceHolder sh) {
		_thread = new GameThread(sh, new MarshmallowLogic());
		setThreadCb();
		_thread.setRunning(true);
		_thread.start();
	}

	@Override
	public void surfaceDestroyed(SurfaceHolder arg0) {
		boolean retry = true;
		_thread.setRunning(false);
		while (retry) {
			try {
				_thread.join();
				retry = false;
			} catch (InterruptedException e) {
			}
		}
	}
	
	@Override
	protected void onDraw(Canvas canvas) {
		// TODO
	}


}
