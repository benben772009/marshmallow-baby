package com.honeymimi.marshmallow;

import com.honeymimi.marshmallow.R;
import com.honeymimi.marshmallow.gameData.MarshmallowStaticData;

import android.support.v4.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RadioButton;

public class ChooselvlActivity extends PortraitActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_chooselvl);

		if (savedInstanceState == null) {
			getSupportFragmentManager().beginTransaction()
					.add(R.id.container, new PlaceholderFragment()).commit();
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.chooselvl, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
	public void onRadioButtonClicked(View view) {
	    // Is the button now checked?
	    boolean checked = ((RadioButton) view).isChecked();
	    
	    if (checked) {
		    switch(view.getId()) {
		        case R.id.radioChooseLvlEasy:
		            MarshmallowStaticData.setDifficult(0);
		            break;
		        case R.id.radioChooseLvlNormal:
		        	MarshmallowStaticData.setDifficult(1);
		            break;
		        case R.id.radioChooseLvlDiff:
		        	MarshmallowStaticData.setDifficult(2);
		            break;
		    }
	    }
	}

	/**
	 * A placeholder fragment containing a simple view.
	 */
	public static class PlaceholderFragment extends Fragment {

		public PlaceholderFragment() {
		}

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {
			View rootView = inflater.inflate(R.layout.fragment_chooselvl,
					container, false);
			Button startButton = (Button) rootView.findViewById(R.id.btnChooselvlStartgame);
			startButton.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					_startGameActivity();
				}
			});
			Button backButton = (Button) rootView.findViewById(R.id.btnChooselvlBack);
			backButton.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					_startMainActivity();
				}
			});
			return rootView;
		}
		
		private void _startGameActivity () {
        	Intent intent = new Intent(getActivity(), GameActivity.class);
        	startActivity(intent);
        }
		
		private void _startMainActivity () {
        	getActivity().finish();
        }
	}

}
