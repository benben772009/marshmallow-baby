package com.honeymimi.marshmallow;

import com.honeymimi.marshmallow.gameData.MarshmallowStaticData;
import com.honeymimi.marshmallow.GameEndActivity;
import com.honeymimi.marshmallow.R;

import android.support.v4.view.GestureDetectorCompat;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.view.GestureDetector;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
//import android.os.Build;
import android.widget.FrameLayout;
import android.widget.TextView;

public class GameActivity extends PortraitActivity 
	implements GestureDetector.OnGestureListener, GestureDetector.OnDoubleTapListener {
	final String DEBUG_TAG = "GsetureEvent";
	private GestureDetectorCompat mDetector;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		Resources res = getResources();
		MarshmallowStaticData.setFontSize(res.getInteger(R.integer.font_size));
		MarshmallowStaticData.setLineHeight(res.getInteger(R.integer.line_height));

		FrameLayout fl = new FrameLayout(this);
		GameSurface gs = new GameSurface(this);
		fl.addView(gs, 
			new FrameLayout.LayoutParams(
				FrameLayout.LayoutParams.MATCH_PARENT,
				FrameLayout.LayoutParams.MATCH_PARENT)
		);
		TextView tv = new TextView(this);
		tv.setGravity(Gravity.RIGHT);
		tv.setTextSize(14);
		MarshmallowStaticData.setScoreTv(tv);
		MarshmallowStaticData.setGameAct(this);
		fl.addView(tv,
			new FrameLayout.LayoutParams(
				FrameLayout.LayoutParams.MATCH_PARENT,
				FrameLayout.LayoutParams.WRAP_CONTENT)
		);
		setContentView(fl);
		gs.setCb(new Runnable() {
			@Override
			public void run() {
				gameOver();
			}
		});
		mDetector = new GestureDetectorCompat(this,this);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.game, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
	@Override 
    public boolean onTouchEvent(MotionEvent event){
//		if (event.getAction() == MotionEvent.ACTION_UP) {
//			MarshmallowStaticData.setRowX(-1);
//		}
		this.mDetector.onTouchEvent(event);
        return super.onTouchEvent(event);
    }

    @Override
    public boolean onDown(MotionEvent event) { 
        return false;
    }

    @Override
    public boolean onFling(MotionEvent event1, MotionEvent event2, 
            float velocityX, float velocityY) {
    	if (velocityY < -1 * MarshmallowStaticData.getFlingMinUp()) {
    		MarshmallowStaticData.setSwipeAction(MarshmallowStaticData.SWIPE_UP);
    	} else if (velocityY > MarshmallowStaticData.getFlingMinDown()) {
    		MarshmallowStaticData.setSwipeAction(MarshmallowStaticData.SWIPE_DOWN);
    	}
        return false;
    }

    @Override
    public void onLongPress(MotionEvent event) {
    	//
    }

    @Override
    public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX,
            float distanceY) {
        return false;
    }

    @Override
    public void onShowPress(MotionEvent event) {
    	//
    }

    @Override
    public boolean onSingleTapUp(MotionEvent event) {
        return true;
    }
    
    @Override
    public boolean onDoubleTap(MotionEvent event) {
    	MarshmallowStaticData.setQuickDrop(true);
        return true;
    }

    @Override
    public boolean onDoubleTapEvent(MotionEvent event) {
        return true;
    }

    @Override
    public boolean onSingleTapConfirmed(MotionEvent event) {
    	MarshmallowStaticData.setRowX((int)event.getX());
        return true;
    }
    
    public void gameOver() {
    	Intent intent = new Intent(this, GameEndActivity.class);
    	startActivity(intent);
    }

}
