package com.honeymimi.marshmallow;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.honeymimi.ads.ToastAdListener;
import com.honeymimi.marshmallow.gameData.MarshmallowStaticData;
import com.honeymimi.marshmallow.ChooselvlActivity;
import com.honeymimi.marshmallow.R;

import android.support.v4.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;

public class MainActivity extends PortraitActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        MarshmallowStaticData.useSettings(
        	getSharedPreferences(MarshmallowStaticData.PREFS_NAME, 0)
        );

        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.container, new PlaceholderFragment())
                    .commit();
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends Fragment {

    	private Button _startButton;
    	private Button _helpButton;
    	private Button _highscoreButton;
    	
    	private AdView _mAdView;
    	
        public PlaceholderFragment() {
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_main, container, false);
            _startButton = (Button) rootView.findViewById(R.id.btnMainStartgame);
            _helpButton = (Button) rootView.findViewById(R.id.button2);
            _highscoreButton = (Button) rootView.findViewById(R.id.button3);
            _setListeners();
            _setUpAds(rootView);
            return rootView;
        }
        
        private void _setUpAds(View rootView) {
        	_mAdView = new AdView(getActivity());
        	_mAdView.setAdUnitId(getResources().getString(R.string.ad_unit_id));
        	_mAdView.setAdSize(AdSize.BANNER);
        	_mAdView.setAdListener(new ToastAdListener(getActivity()));
        	FrameLayout layout = (FrameLayout) rootView.findViewById(R.id.frameLayoutMain);
        	FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(
        		FrameLayout.LayoutParams.MATCH_PARENT,
        		FrameLayout.LayoutParams.WRAP_CONTENT
        	);
            layout.addView(_mAdView, params);
        	_mAdView.loadAd(new AdRequest.Builder().build());
        }
        
        private void _setListeners () {
        	_startButton.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					_startGameActivity();
				}
			});
        	_helpButton.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					_startHelpActivity();
				}
			});
        	_highscoreButton.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					_startHighscoreActivity();
				}
			});
        }
        
        private void _startHelpActivity () {
        	Intent intent = new Intent(getActivity(), HelpActivity.class);
        	startActivity(intent);
        }
        
        private void _startGameActivity () {
        	Intent intent = new Intent(getActivity(), ChooselvlActivity.class);
        	startActivity(intent);
        }
        
        private void _startHighscoreActivity () {
        	Intent intent = new Intent(getActivity(), HighscoreActivity.class);
        	startActivity(intent);
        }
        
        @Override
        public void onPause() {
            _mAdView.pause();
            super.onPause();
        }

        @Override
        public void onResume() {
            super.onResume();
            _mAdView.resume();
        }

        @Override
        public void onDestroy() {
            _mAdView.destroy();
            super.onDestroy();
        }
        
        // TODO: remove listeners on destroy
    }

}
