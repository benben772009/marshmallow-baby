package com.honeymimi.marshmallow;

import com.honeymimi.marshmallow.R;
import com.honeymimi.marshmallow.gameData.MarshmallowStaticData;

import android.support.v4.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

public class GameEndActivity extends PortraitActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_game_end);

		if (savedInstanceState == null) {
			getSupportFragmentManager().beginTransaction()
					.add(R.id.container, new PlaceholderFragment()).commit();
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.game_end, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	/**
	 * A placeholder fragment containing a simple view.
	 */
	public static class PlaceholderFragment extends Fragment {
		
		private Button _backButton;

		public PlaceholderFragment() {
		}

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {
			View rootView = inflater.inflate(R.layout.fragment_game_end,
					container, false);
			_backButton = (Button) rootView.findViewById(R.id.btnHighscoreBack);
			_backButton.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					_startMainActivity();
				}
			});
			TextView tv = (TextView) rootView.findViewById(R.id.gameEndScore);
			tv.setText(String.valueOf(MarshmallowStaticData.getLastestScore()));
			return rootView;
		}
		
		private void _startMainActivity () {
			Intent intent = new Intent(getActivity(), MainActivity.class);
		    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);   
		    startActivity(intent);
        }
	}

}
