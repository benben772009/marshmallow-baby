package com.honeymimi.marshmallow.gameData;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;

import com.honeymimi.utils.IGameData;

public class MarshmallowLogic implements IGameData {
	private MarshmallowBoard _board;
	private MarshmallowColumn _column = null;
	private MarshmallowColumn _nextColumn = null;
	private MarshmallowScore _score;
	private boolean _isFinished = false;
	private int _countDown = 0;
	private int _height = MarshmallowStaticData.getHeight();
	private int _status = 0; // 0: normal, 1: waitForRemove, 2: waitForDropDown
	private int _lineX = -1;
	private Paint _linePaint;
	
	public MarshmallowLogic () {
		_linePaint = new Paint();
		_linePaint.setColor( Color.LTGRAY);
		_board = new MarshmallowBoard(MarshmallowStaticData.getWidth(), MarshmallowStaticData.getHeight());
		_score = new MarshmallowScore(MarshmallowStaticData.getHighestScores());
		MarshmallowStaticData.resetLvl();
	}
	
	private void _generateColumn() {
		int x1 = _board.getWidth();
		int x2 = (int)(_board.getWidth()/2);
		if (_nextColumn != null) {
			_column = _nextColumn;
			_column.setX(x2);
			_column.setY(0);
			_nextColumn = new MarshmallowColumn(x1, 3, MarshmallowStaticData.getColumnHeight());
		} else {
			_column = new MarshmallowColumn(x2, 0, MarshmallowStaticData.getColumnHeight());
			_nextColumn = new MarshmallowColumn(x1, 3, MarshmallowStaticData.getColumnHeight());
		}
	}
	
	private boolean _checkColumnLanded() {
		return !(_column.getY() < _height - 1 && _board.getPoint(_column.getX(), _column.getY()+1) == null);
	}
	
	private boolean _canMoveTo(int tRowX) {
		if (_countDown == 0) { // avoid some unexpected bugs
			return false;
		}
		int x = _column.getX();
		int y = _column.getY();
		if (tRowX == x) {
			return false;
		} else if (tRowX > x) {
			for (; tRowX > x; tRowX--) {
				if (_board.getPoint(tRowX, y) != null) {
					return false;
				}
			}
		} else {
			for (; tRowX < x; tRowX++) {
				if (_board.getPoint(tRowX, y) != null) {
					return false;
				}
			}
		}
		return true;
	}
	
	public void update () {
		// user input
		int tRowX = MarshmallowStaticData.getRowX(); // h move
		if (tRowX != -1) {
			if (_column != null && tRowX >= 0 && tRowX < MarshmallowStaticData.getWidth() && _canMoveTo(tRowX) == true) {
				_column.setX(tRowX);
			}
			MarshmallowStaticData.setRowX(-1);
		} // swipe
		if (_column != null && MarshmallowStaticData.getSwipeAction() != MarshmallowStaticData.SWIPE_NONE) {
			_column.swipe(MarshmallowStaticData.getSwipeAction());
			MarshmallowStaticData.setSwipeAction(MarshmallowStaticData.SWIPE_NONE);
		}
		// game logic
		if (MarshmallowStaticData.getQuickDrop() == true
				&& _status == 0
				&& _column != null) {
			_countDown = 0; // quick drop down, set _countDown to 0
		}
		if (_countDown == 0) {
			if (_status == 1) {
				_board.dropDownTiles();
				_status = 2;
				_countDown = MarshmallowStaticData.getDropDownTilesCd();
			} else if (_status == 2) {
				this._status = 0;
				_checkRemoveTiles();
			} else if (_column == null) { // below: _status == 0
				_generateColumn();
				_countDown = MarshmallowStaticData.getDropCd();
			} else if (MarshmallowStaticData.getQuickDrop() == true) { // user input: quick drop
				_column.setY(_board.getQuickDropPosition(_column.getX()));
				_countDown = MarshmallowStaticData.getQuickDropNextCd();
				MarshmallowStaticData.setQuickDrop(false);
			} else if (_checkColumnLanded() == false) {
				_column.setY(_column.getY()+1);
				_countDown = MarshmallowStaticData.getDropCd();
			} else {
				_isFinished = !_board.landColumn(_column);
				if (_isFinished) {
					MarshmallowStaticData.addHighScores(_score.getScore());
					return; // TODO: game over
				}
				if (_column.getPointAt(0).getColor() == 8
					|| _column.getPointAt(0).getColor() == 9) { // todo: we could let _board give this info
					this._status = 1;
					_countDown = MarshmallowStaticData.getRemoveTilesCd();
					MarshmallowStaticData.insContinuesRemove();
				}
				_column = null;
				_checkRemoveTiles();
			}
		}
		--_countDown;
	}

	private void _checkRemoveTiles() {
		int score = _board.findTilesToRemove();
		if (score > 0) {
			_score.addScore(score);
			this._status = 1;
			_countDown = MarshmallowStaticData.getRemoveTilesCd();
			MarshmallowStaticData.insContinuesRemove();
		} else {
			_countDown = MarshmallowStaticData.getRegenerateColumnCd();
			MarshmallowStaticData.resetContinuesRemove();
		}
	}
	
	public void doDraw (Canvas c) {
		if (c != null) {
			try {
				c.restore();
			} catch (Exception e) {
				// Android-4.3:  no .save() called before restore
			}
			c.drawColor(MarshmallowStaticData.getBgColor());
			if (_column != null) {
				_column.draw(c);
			}
			if (_nextColumn != null) {
				_nextColumn.draw(c);
			}
			_board.draw(c);
			if (_lineX == -1) {
				_lineX = MarshmallowStaticData.getPaddingH()
					+ MarshmallowStaticData.getTileSize() * MarshmallowStaticData.getWidth() + 2;
			}
			c.drawLine(_lineX, MarshmallowStaticData.getPaddingTop(),
					_lineX, c.getHeight(), _linePaint);
			c.save();
		}
	}
	
	public boolean isFinished () {
		return _isFinished;
	}
	
	public int getInterval () {
		return MarshmallowStaticData.getInterval();
	}
}
