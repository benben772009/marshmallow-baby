package com.honeymimi.marshmallow.gameData;

import java.util.ArrayList;

import android.graphics.Canvas;

import com.honeymimi.utils.IDrawable;

public class MarshmallowBoard implements IDrawable {
	private int _width;
	private int _height;
	private MarshmallowPoint _points[][];
	private int _scoreToIncrease;
	
	public MarshmallowBoard (int width, int height) {
		_width = width;
		_height = height;
		_initBoards();
	}
	
	private void _initBoards () {
		_points = new MarshmallowPoint[_width][_height];
		for (int i = 0; i < _width; i++) {
			for (int j = 0; j < _height; j++) {
				_points[i][j] = null;
			}
		}
	}
	
	/*
	 * return false when game failed
	 */
	public boolean landColumn (MarshmallowColumn column) {
		int x = column.getX();
		int y = column.getY();
		int height = column.getHeight();
		int cy;
		int color = column.getPointAt(0).getColor();
		if (color == 8) {
			int count = 6;
			for (int j = _height - 1; j >= 0 && count > 0; j--) {
				MarshmallowPoint p = _points[x][j];
				if (p != null) {
					p.setToDelete();
					--count;
				}
			}
		} else if (color == 9) {
			for (int i = x-1; i <= x+1; i++) {
				for (int j = y-1; j <= y+1; j++) {
					try {
						MarshmallowPoint p = _points[i][j];
						p.setToDelete();
					} catch (Exception e) {
						// TODO: handle exception
					}
				}
			}
		} else {
			for (int i = 0; i < height; i++) {
				cy = y - i;
				if (cy >= 0) {
					_points[x][cy] = column.getPointAt(i);
					_points[x][cy].setToLanded();
				} else {
					return false;
				}
			}
		}
		return true;
	}

	public MarshmallowPoint getPoint (int x, int y) {
		MarshmallowPoint point = _points[x][y];
		return point;
	}
	
	public int getQuickDropPosition (int x) {
		for (int j = _height - 1; j >= 0; j--) {
			if (getPoint(x, j) == null) {
				return j;
			}
		}
		return 0;
	}
	
	public int getWidth () {
		return _width;
	}
	
	public int getHeight () {
		return _height;
	}

	@Override
	public void draw(Canvas c) {
		for (int i = 0; i < _width; i++) {
			for (int j = 0; j < _height; j++) {
				MarshmallowPoint point = _points[i][j];
				if (point != null) {
					point.draw(c);
				}
			}
		}
	}
	
	private void _findHMatch() {
		ArrayList<MarshmallowPoint> list = new ArrayList<MarshmallowPoint>();
		int previousC = 0;
		MarshmallowPoint currentP;
		for (int j = _height - 1; j >= 0; j--) {
			previousC = 0;
			list.clear();
			for (int i = 0; i < _width; i++) {
				currentP = _points[i][j];
				if (previousC != 0 && currentP != null && currentP.getColor() == previousC) {
					list.add(currentP);
				} else {
					_checkTileScore(list);
					list.clear();
					if (currentP != null) {
						previousC = currentP.getColor();
						list.add(currentP);
					} else {
						previousC = 0;
					}
				}
			}
			_checkTileScore(list);
		}
	}
	
	private void _findVMatch() {
		ArrayList<MarshmallowPoint> list = new ArrayList<MarshmallowPoint>();
		int previousC = 0;
		MarshmallowPoint currentP;
		for (int i = 0; i < _width; i++) {
			previousC = 0;
			list.clear();
			for (int j = _height - 1; j >= 0; j--) {
				currentP = _points[i][j];
				if (previousC != 0 && currentP != null && currentP.getColor() == previousC) {
					list.add(currentP);
				} else {
					_checkTileScore(list);
					list.clear();
					if (currentP != null) {
						previousC = currentP.getColor();
						list.add(currentP);
					} else {
						previousC = 0;
					}
				}
			}
			_checkTileScore(list);
		}
	}
	
	private void _findTopLeftMatch () {
		ArrayList<MarshmallowPoint> list = new ArrayList<MarshmallowPoint>();
		int previousC = 0;
		MarshmallowPoint currentP;
		int i;
		int j;
		int tmp;
		for (i = 0; i < _width; i++) {
			tmp = i;
			previousC = 0;
			list.clear();
			for (j = 0; j < _height && tmp < _width; ) {
				currentP = _points[tmp][j];
				if (previousC != 0 && currentP != null && currentP.getColor() == previousC) {
					list.add(currentP);
				} else {
					_checkTileScore(list);
					list.clear();
					if (currentP != null) {
						previousC = currentP.getColor();
						list.add(currentP);
					} else {
						previousC = 0;
					}
				}
				j++;
				tmp++;
			}
			_checkTileScore(list);
		}
		
		for (j = 1; j < _height; j++) {
			tmp = j;
			previousC = 0;
			list.clear();
			for (i = 0; tmp < _height && i < _width; ) {
				currentP = _points[i][tmp];
				if (previousC != 0 && currentP != null && currentP.getColor() == previousC) {
					list.add(currentP);
				} else {
					_checkTileScore(list);
					list.clear();
					if (currentP != null) {
						previousC = currentP.getColor();
						list.add(currentP);
					} else {
						previousC = 0;
					}
				}
				i++;
				tmp++;
			}
			_checkTileScore(list);
		}
	}
	
	private void _findDownLeftMatch () {
		ArrayList<MarshmallowPoint> list = new ArrayList<MarshmallowPoint>();
		int previousC = 0;
		MarshmallowPoint currentP;
		int i;
		int j;
		int tmp;
		for (i = 0; i < _width; i++) {
			tmp = i;
			previousC = 0;
			list.clear();
			for (j = _height - 1; j >= 0 && tmp < _width; ) {
				currentP = _points[tmp][j];
				if (previousC != 0 && currentP != null && currentP.getColor() == previousC) {
					list.add(currentP);
				} else {
					_checkTileScore(list);
					list.clear();
					if (currentP != null) {
						previousC = currentP.getColor();
						list.add(currentP);
					} else {
						previousC = 0;
					}
				}
				j--;
				tmp++;
			}
			_checkTileScore(list);
		}
		
		for (j = _height-2; j >=0; j--) {
			tmp = j;
			previousC = 0;
			list.clear();
			for (i = 0; tmp >= 0 && i < _width; ) {
				currentP = _points[i][tmp];
				if (previousC != 0 && currentP != null && currentP.getColor() == previousC) {
					list.add(currentP);
				} else {
					_checkTileScore(list);
					list.clear();
					if (currentP != null) {
						previousC = currentP.getColor();
						list.add(currentP);
					} else {
						previousC = 0;
					}
				}
				i++;
				tmp--;
			}
			_checkTileScore(list);
		}
	}
	
	private void _checkTileScore (ArrayList<MarshmallowPoint> list) {
		if (list.size() >= 3) {
			_scoreToIncrease += MarshmallowStaticData.getMatchScore(list.size());
			for (MarshmallowPoint p : list) {
				p.setToDelete();
			}
		}
	}
	
	public int findTilesToRemove () {
		_scoreToIncrease = 0;
		_findHMatch();
		_findVMatch();
		_findTopLeftMatch();
		_findDownLeftMatch();
		return _scoreToIncrease;
	}

	public void dropDownTiles() {
		MarshmallowPoint p;
		int pos;
		for (int i = 0; i < _width; i++) {
			pos = 0;
			for (int j = _height - 1; j >= 0; j--) {
				p = _points[i][j];
				if (p == null) {
					break;
				} else if (p.getToDelete() == true) {
					_points[i][j] = null;
					++pos;
				} else if (pos != 0) {
					p.setY(j+pos);
					_points[i][j+pos] = p;
					_points[i][j] = null;
				}
			}
		}
	}
}
