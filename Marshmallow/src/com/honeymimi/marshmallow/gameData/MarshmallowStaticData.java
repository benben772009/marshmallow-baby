package com.honeymimi.marshmallow.gameData;

import android.app.Activity;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.widget.TextView;

public class MarshmallowStaticData {
	public static final String PREFS_NAME = "MarshmallowBabyPref";
	private static final int LVL_SCORES[] = {
		2000,
		5000
	};
	private static final int LVL_SPEED[] = {
		10,
		8,
		6
	};
	
	public static final int SWIPE_NONE = 0;
	public static final int SWIPE_UP = -1;
	public static final int SWIPE_DOWN = 1;
	
	private static int _tileSize = -1;
	private static int _swipeAction = SWIPE_NONE;
	private static int _rowX= -1;
	private static boolean _quickDrop = false;
	private static int _lvl = 0;
	private static int _latestScore = -1;
	private static int _diff = 0;
	private static int _colorCount = 5;
	private static int _lineHeight = -1;
	private static int _fontSize = -1;
	
	private static Bitmap _b;
	private static int _btSize;
	private static int _continuesRemove = 0;
	private static TextView _scoreTv;
	private static Activity _gameAct;
	
	private static SharedPreferences _settings;

	public static int getWidth () {
		return 6;
	}
	
	public static int getHeight () {
		return 12;
	}
	
	public static void setDifficult (int diff) {
		_diff = diff;
		switch(diff){
		case 0:
			_colorCount = 5;
			break;
		case 1:
			_colorCount = 6;
			break;
		case 2:
			_colorCount = 7;
			break;
		}
	}
	
	public static int getColorCount () {
		return _colorCount;
	}
	
	public static int getColumnHeight () {
		return 3;
	}
	
	public static int getPaddingTop() {
		return 50;
	}
	
	public static int getPaddingH() {
		return 10;
	}
	
	public static int getTileSize() {
		return _tileSize;
	}
	
	public static void setTileSize(int tileSize) {
		_tileSize = tileSize;
	}
	
	public static int getInterval () {
		return 50;
	}
	
	public static int getDropCd () {
		return LVL_SPEED[_lvl];
	}
	
	public static int getRegenerateColumnCd () {
		return 2;
	}
	
	public static int getQuickDropNextCd () {
		return 3;
	}
	
	public static int getRemoveTilesCd () {
		return 5;
	}
	
	public static int getDropDownTilesCd() {
		return 5;
	}
	
	public static int getFlingMinUp() {
		return 500;
	}
	
	public static int getFlingMinDown() {
		return 400;
	}
	
	public static int getBgColor() {
		return 0xFFFAE3BB;
	}
	
	public static void setSwipeAction(int action) {
		_swipeAction = action;
	}
	
	public static int getSwipeAction() {
		return _swipeAction;
	}
	
	public static void setRowX(int x) {
		if (x == -1) {
			_rowX = -1;
		} else {
			_rowX = Math.round((x - getPaddingH()) / getTileSize());
		}
	}
	
	public static int getRowX() {
		return _rowX;
	}
	
	public static int getMatchScore(int count) {
		switch (count) {
		case 3:
			return 30 + _continuesRemove * 30;
		case 4:
			return 45 + _continuesRemove * 45;
		case 5:
			return 60 + _continuesRemove * 60;
		default:
			return 0;
		}
	}
	
	public static void setQuickDrop (boolean flag) {
		_quickDrop = flag;
	}
	
	public static boolean getQuickDrop () {
		return _quickDrop;
	}
	
	public static void setBitmap(Bitmap b) {
		_b = b;
		_btSize = (int)(_b.getWidth()/3);
	}
	
	public static Bitmap getBitmap() {
		return _b;
	}
	
	public static int getBtSize() {
		return _btSize;
	}
	
	public static void insContinuesRemove() {
		++_continuesRemove;
	}
	
	public static void resetContinuesRemove() {
		_continuesRemove = 0;
	}
	
	public static void useSettings(SharedPreferences s) {
		_settings = s;
	}
	
	public static int getHighestScores() {
		String key;
		if (_diff == 0) {
			key = "hightscrore_" + 0;
		} else {
			key = "hightscrore_" + _diff + "_" + 0;
		}
		return  _settings.getInt(key, 0);
	}
	
	public static int[] getHighScores(int diff) {
		int arr[] = new int[5];
		String key;
		for (int i = 0; i < 5; i++) {
			if (diff == 0) { // keep the old data
				key = "hightscrore_" + i;
			} else {
				key = "hightscrore_" + diff + "_" + i;
			}
			arr[i] = _settings.getInt(key, 0);
		}
		return arr;
	}
	
	public static int[] addHighScores(int newHighScore) {
		_latestScore = newHighScore;
		SharedPreferences.Editor editor = _settings.edit();
		int arr[] = new int[5];
		int score;
		String key;
		for (int i = 0; i < 5; i++) {
			if (_diff == 0) { // keep the old data
				key = "hightscrore_" + i;
			} else {
				key = "hightscrore_" + _diff + "_" + i;
			}
			score = _settings.getInt(key, 0);
			if (newHighScore > score) {
				arr[i] = newHighScore;
				editor.putInt(key, newHighScore);
				newHighScore = score;
			} else {
				arr[i] = score;
			}
		}
		editor.commit();
		return arr;
	}
	
	public static int getLastestScore() {
		return _latestScore;
	}
	
	public static void resetLvl() {
		_lvl = 0;
		_latestScore = -1;
	}
	
	public static void lvlUp(int score) {
		if (_lvl < LVL_SCORES.length) {
			if (score >= LVL_SCORES[_lvl]) {
				++_lvl;
			}
		}
	}
	
	public static int getLvl() {
		return _lvl;
	}
	
	public static void setFontSize(int fontSize) {
		_fontSize = fontSize;
	}
	
	public static int getFontSize() {
		return _fontSize;
	}
	
	public static void setLineHeight(int lineHeight) {
		_lineHeight = lineHeight;
	}
	
	public static int getLineHeight() {
		return _lineHeight;
	}
	
	public static void setScoreTv(TextView tv) {
		_scoreTv = tv;
	}
	
	public static TextView getScoreTv() {
		return _scoreTv;
	}
	
	public static void setGameAct(Activity act) {
		_gameAct = act;
	}
	
	public static Activity getGameAct() {
		return _gameAct;
	}
}
