package com.honeymimi.marshmallow.gameData;

import android.widget.TextView;


public class MarshmallowScore{
	private int _score = 0;
	private int _highestScore;
	private TextView _scoreTv;
	private String _str;
	
	public MarshmallowScore (int highestScore) {
		_highestScore = highestScore;
		_scoreTv = MarshmallowStaticData.getScoreTv();
		_str = "HIGHSCORE: "+_highestScore+"  SCORE: "+_score;
		MarshmallowStaticData.getGameAct().runOnUiThread(new Runnable() {
            @Override
            public void run() {
            	_scoreTv.setText(_str);
            }
        });
	}
	
	public void addScore (int scoreInc) {
		_score += scoreInc;
		_str = "HIGHSCORE: "+_highestScore+"  SCORE: "+_score;
		MarshmallowStaticData.getGameAct().runOnUiThread(new Runnable() {
            @Override
            public void run() {
            	_scoreTv.setText(_str);
            }
        });
		MarshmallowStaticData.lvlUp(_score);
	}
	
	public int getScore () {
		return _score;
	}
}
