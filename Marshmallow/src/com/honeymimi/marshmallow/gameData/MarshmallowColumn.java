package com.honeymimi.marshmallow.gameData;

import android.graphics.Canvas;

import com.honeymimi.utils.IDrawable;
import com.honeymimi.utils.Point;

public class MarshmallowColumn extends Point implements IDrawable {
	private static int _countDown = 20;

	private MarshmallowPoint _points[];
	
	/*
	 * (x,y) is the bottom point of a column
	 */
	public MarshmallowColumn (int x, int y, int height) {
		super(x, y);
		
		_points = new MarshmallowPoint[height];
		_generatePoints();
	}
	
	private void _generatePoints () {
		if (_countDown == 0) {
			if (Math.random() > 0.5) {
				for (int i = 0; i < _points.length; i++) {
					_points[i] = new MarshmallowPoint(_x, _y - i, 8);
				}
			} else {
				for (int i = 0; i < _points.length; i++) {
					_points[i] = new MarshmallowPoint(_x, _y - i, 9);
				}
			}
			_countDown = (int)(Math.random()*10) + 25;
		} else {
			for (int i = 0; i < _points.length; i++) {
				_points[i] = new MarshmallowPoint(_x, _y - i, (int)(MarshmallowStaticData.getColorCount() * Math.random()) + 1);
			}
			while (_sameColor() == true) {
				_points[0].setColor((int)(MarshmallowStaticData.getColorCount() * Math.random()) + 1);
			}
			_countDown--;
		}
	}
	
	private boolean _sameColor () {
		int previousC = _points[0].getColor();
		for (int i = 1; i < _points.length; i++) {
			if (_points[i].getColor() != previousC) {
				return false;
			}
		}
		return true;
	}
	
	@Override
	public void setX(int x) {
		super.setX(x);
		for (int i = 0; i < _points.length; i++) {
			_points[i].setX(x);
		}
	}
	
	@Override
	public void setY(int y) {
		super.setY(y);
		for (int i = 0; i < _points.length; i++) {
			_points[i].setY(y - i);
		}
	}
	
	public int getHeight () {
		return _points.length;
	}
	
	public void swipe(int action) {
		int[] colors = new int[_points.length];
		if (action == MarshmallowStaticData.SWIPE_UP) {
			for (int i = 0; i < _points.length; i++) {
				if (i != 0) {
					colors[i] = _points[i - 1].getColor();
				} else {
					colors[i] = _points[_points.length - 1].getColor();
				}
			}
		} else if (action == MarshmallowStaticData.SWIPE_DOWN) {
			for (int i = 0; i < _points.length; i++) {
				if (i != _points.length - 1) {
					colors[i] = _points[i + 1].getColor();
				} else {
					colors[i] = _points[0].getColor();
				}
			}
		}
		for (int i = 0; i < _points.length; i ++) {
			_points[i].setColor(colors[i]);
		}
	}
	
	/*
	 * pos starts from 0, bottom to top
	 */
	public MarshmallowPoint getPointAt (int pos) {
		return _points[pos];
	}

	@Override
	public void draw(Canvas c) {
		for (int i = 0; i < _points.length; i++) {
			_points[i].draw(c);
		}
	}

}
