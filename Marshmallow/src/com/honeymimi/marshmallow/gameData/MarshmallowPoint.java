package com.honeymimi.marshmallow.gameData;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;

import com.honeymimi.utils.IDrawable;
import com.honeymimi.utils.Point;

public class MarshmallowPoint extends Point implements IDrawable {
	private Paint _paint;
	private int _color;
	private int _status = 0; // 0: normal, 1: toDelete, 2: landed
	private Rect _rect;
	private static int _canvasWidth = -1;
	private static int _canvasHeight = -1;
	private static int _paddingH = MarshmallowStaticData.getPaddingH();
	private static int _paddingTop = MarshmallowStaticData.getPaddingTop();
	private static int _paddingV = -1;
	private static int _tileSize = -1;
	private static Bitmap _b = MarshmallowStaticData.getBitmap();
	private static int _btSize = MarshmallowStaticData.getBtSize();
	
	public MarshmallowPoint (int x, int y, int color) {
		super(x, y);
		_color = color;
		_paint = new Paint(Paint.ANTI_ALIAS_FLAG);
		if (color == 8) {
			_rect = new Rect(0, 7*_btSize, _btSize, 8*_btSize);
		} else if (color == 9) {
			_rect = new Rect(_btSize, 7*_btSize, 2*_btSize, 8*_btSize);
		} else {
			_rect = new Rect(0, _color*_btSize-_btSize, _btSize, _color*_btSize);
		}
	}
	
	public void setColor (int color) {
		_color = color;
		_rect.top = _color*_btSize-_btSize;
		_rect.bottom = _color*_btSize;
	}
	
	public int getColor () {
		return _color;
	}

	@Override
	public void draw(Canvas c) {
		if (_canvasWidth == -1) {
			_canvasWidth = c.getWidth();
			_canvasHeight = c.getHeight();
			// leave a space on right to show the 'next column'
			int tileSize1 = (int)((_canvasWidth-_paddingH*2)/(MarshmallowStaticData.getWidth()+1));
			int tileSize2 = (int)((_canvasHeight-_paddingTop)/(MarshmallowStaticData.getHeight()));
			int tileSize = tileSize1 > tileSize2 ? tileSize2 : tileSize1 ;
			MarshmallowStaticData.setTileSize(tileSize);
			_tileSize = MarshmallowStaticData.getTileSize();
			_paddingV = _canvasHeight - MarshmallowStaticData.getTileSize() * MarshmallowStaticData.getHeight();
		}
		int tileSize = _tileSize;
		int _sx = _paddingH+_tileSize*_x;
		int _sy = _paddingV+_tileSize*_y;
		if (_x >= MarshmallowStaticData.getWidth()) {
			tileSize -= 10;
			_sx += 5;
			_sy -= _y * 10;
		}
		c.drawBitmap(_b, _rect, new Rect(_sx, _sy, _sx + tileSize, _sy + tileSize), _paint);
	}

	public void setToDelete () {
		if (_color != 8 && _color != 9) {
			_rect.left = _btSize*2;
			_rect.right = _btSize*3;
		}
		_status = 1;
	}
	
	public void setToLanded () {
		if (_color != 8 && _color != 9) {
			_rect.left = _btSize*1;
			_rect.right = _btSize*2;
		}
		_status = 2;
	}
	
	public boolean getToDelete () {
		return _status == 1;
	}
}
