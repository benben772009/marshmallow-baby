package com.honeymimi.marshmallow;

import com.honeymimi.marshmallow.gameData.MarshmallowStaticData;
import com.honeymimi.marshmallow.R;

import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.Button;
import android.widget.TextView;

public class HighscoreActivity extends PortraitActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_highscore);

		if (savedInstanceState == null) {
			getSupportFragmentManager().beginTransaction()
					.add(R.id.container, new PlaceholderFragment()).commit();
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.highscore, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	/**
	 * A placeholder fragment containing a simple view.
	 */
	public static class PlaceholderFragment extends Fragment {

		private Button _backButton;

		public PlaceholderFragment() {
		}

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {
			View rootView = inflater.inflate(R.layout.fragment_highscore,
					container, false);
			ViewGroup highscoreWrapper = (ViewGroup) rootView.findViewById(R.id.highscoreWrapper);
			String score;
			TextView tv;
			int highScore[];
			for (int diff = 0; diff < 3; diff++) {
				highScore = MarshmallowStaticData.getHighScores(diff);
				tv = new TextView(getActivity());
				tv.setText(diffStr(diff));
				tv.setTextSize(20);
				tv.setGravity(Gravity.CENTER_HORIZONTAL);
				tv.setLayoutParams(
						new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));
				highscoreWrapper.addView(tv);
				for (int i = 0; i < 3; i++) { // highScore.length
					score = highScore[i] + "";
					// TODO: or we should append new TextView instead
					tv = new TextView(getActivity());
					tv.setText(score);
					tv.setTextColor(getResources().getColor(R.color.btn_txt));
					tv.setTextSize(18);
					tv.setGravity(Gravity.CENTER_HORIZONTAL);
					tv.setLayoutParams(
							new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));
					highscoreWrapper.addView(tv);
				}
			}
			_backButton = (Button) rootView.findViewById(R.id.button4);
			_backButton.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					_startMainActivity();
				}
			});
			return rootView;
		}
		
		private String diffStr(int diff) {
			switch(diff){
			case 0:
				return getResources().getString(R.string.diffEasy);
			case 1:
				return getResources().getString(R.string.diffNormal);
			case 2:
			default:
				return getResources().getString(R.string.diffHard);
			}
		}
		
		private void _startMainActivity () {
        	getActivity().finish();
        }
	}

}
